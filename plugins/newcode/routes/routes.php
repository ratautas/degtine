<?php

use Newcode\Ui\Models\Ui;
use Newcode\Pages\Models\Page;
use Newcode\Pages\Models\Subpage;
use Newcode\Careers\Models\Career;
use Newcode\Products\Models\Filter;
use Newcode\Products\Models\Value;
use Newcode\Products\Models\Category;
use Newcode\Navigation\Models\Navigation;
use Newcode\Navigation\Models\Mobile;
use Newcode\Navigation\Models\Beverage;
use Newcode\News\Models\News;

Route::get('/', function (){
  return Redirect::to('/lt');
});

Route::get('/endpoint/{lng}', function ($lng){
    if($lng == 'lt' || $lng == 'en' || $lng == 'ru'){
        //implement translate plugin
        $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

        //get
        $pages = 'pages';
        $about = 'about';
        $ui = getUi($lng);
        $nav = getNav($lng);
        $mob_nav = getMobNav($lng);
        $pages = getPages($lng);
        $filters = getFilters($lng);
        $categories = getProductsCategories($lng);
        $news = getNews($lng);

        //create main object
        $object = createObject($pages, $about, $ui, $nav, $mob_nav, $filters, $categories, $news);

        //print json
        return printJson($object);
    }
    else{
        return Redirect::to('404');
    }

});


function createObject($pages, $about, $ui, $nav, $mob_nav, $filters, $categories, $news){
    $object = new stdClass;
    $object->pages = $pages;
    $object->about = $about;
    $object->ui = $ui;
    $object->nav = $nav;
    $object->mobNav = $mob_nav;
    $object->filters = $filters;
    $object->categories = $categories;
    $object->news = $news;
    return $object;
}

function getUi($lng){
    $ui = Ui::first()->lang($lng);
    return $ui;
}

function getPages($lng){

    $pages = Page::get();
    $page_object = new stdClass;

    foreach ($pages as $page){
        $page->lang($lng);
        $page_content_object = '';
        if($page->template == 'HomeTemplate'){
            $page_content_object = getContentForHomeTemplate($page, $lng);
        }
        elseif($page->template == 'SimpleTemplate'){
            $page_content_object = getContentForSimpleTemplate($page, $lng);
        }
        elseif($page->template == 'CareerTemplate'){
            $page_content_object = getContentForCareerTemplate($page, $lng);
        }
        elseif($page->template == 'ContactsTemplate'){
            $page_content_object = getContentForContactsTemplate($page, $lng);
        }
        elseif($page->template == 'PageWithSubpages'){
            $page_content_object = getContentForPageWithSubpages($page, $lng);
        }

        $page_slug = $page->slug;
        $page_object->$page_slug = $page_content_object;
    }

    return $page_object;
}

function getFilters($lng){
    $filters = Filter::get();
    foreach ($filters as $filter){
        $filter->lang($lng);
        $filter_id = $filter->id;
        $values = Value::where('filter_id', $filter_id)->get();
        if($values){
            foreach ($values as $value){
                $value->lang($lng);
            }
        }
        $filter->values = $values;
    }
    return $filters;
}

function getProductsCategories($lng){
    $category_object = new stdClass;
    $categories = Category::get();

    foreach ($categories as $category){
        $category->lang($lng);
        $products_object = new stdClass;
        $category_slug = $category->slug;
        $category_products = $category->products;
        foreach ($category_products as $product){
            if($lng == 'lt' && $product->show_lt == 1 || $lng == 'en' && $product->show_en == 1 || $lng == 'ru' && $product->show_ru == 1){
                $product->lang($lng);
                if($product->description == '---'){
                    $product->description = '';
                }
                $product_slug = $product->slug;
                $products_object->$product_slug = $product;

                if($lng == 'ru'){
                    $products_object->$product_slug->left_section = $products_object->$product_slug->left_section_ru;
                    $products_object->$product_slug->right_section = $products_object->$product_slug->right_section_ru;
                    $products_object->$product_slug->sizes = $products_object->$product_slug->sizes_ru;
                }
                else if($lng == 'en'){
                    $products_object->$product_slug->left_section = $products_object->$product_slug->left_section_en;
                    $products_object->$product_slug->right_section = $products_object->$product_slug->right_section_en;
                    $products_object->$product_slug->sizes = $products_object->$product_slug->sizes_en;
                }

                unset($products_object->$product_slug->left_section_en);
                unset($products_object->$product_slug->right_section_en);
                unset($products_object->$product_slug->left_section_ru);
                unset($products_object->$product_slug->right_section_ru);
                unset($products_object->$product_slug->sizes_en);
                unset($products_object->$product_slug->sizes_ru);


                if($product->filters){
                    $product_filters_obj = new stdClass();

                    foreach ($product->filters as $filter){
                        $filter_first_obj = Filter::where('id', $filter['filter'])->first();
                        if (!$filter_first_obj) continue;
                        $filter_obj = $filter_first_obj->lang($lng);
                        $val_first_obj = Value::where('id', $filter['value'])->first();
                        if (!$val_first_obj) continue;
                        $value_obj = $val_first_obj->lang($lng);

                        $filter_obj_name = '';
                        $value_obj_label = '';
                        if($filter_obj && $value_obj){
                            $filter_obj_name = $filter_obj->title;
                            $value_obj_label = $value_obj->value;
                        }

                        if($filter_obj_name != '' && $value_obj_label != ''){
                            $product_filters_obj->$filter_obj_name = $value_obj_label;
                        }

                    }

                    $product->filters = $product_filters_obj;
                }
            }
        }

        $filters_object = new stdClass;
        $category_filters = $category->filters;
        foreach ($category_filters as $filter){
            $filter->lang($lng);
            $filter_title = $filter->title;
            $filters_object->$filter_title = $filter;

            $filter_title_object = $filters_object->$filter_title;
            $filter_values_object = new stdClass;
            $filter_values = $category->values;
            foreach ($filter_values as $value){
                $value->lang($lng);
                $filter_id = $value->filter_id;
                if($filter_id == $filter->id){
                    $filter_value = $value->value;
                    $filter_values_object->$filter_value = $value;
                }
            }
            $filter_title_object->values = $filter_values_object;
        }

        $category_beverages = new stdClass();
        $category_beverages->beverages = $products_object;
        $category_object->$category_slug = $category_beverages;

        $category_slug_object = $category_object->$category_slug;
        $category_slug_object->filters = $filters_object;

        //category content
        $category_slug_object->meta_title = $category->meta_title;
        $category_slug_object->meta_description = $category->meta_description;
        $category_slug_object->meta_image = $category->meta_image;
        $category_slug_object->title = $category->title;
        $category_slug_object->color = $category->color;
        $category_slug_object->template = $category->template;
        $category_slug_object->slug = $category->slug;
        $category_slug_object->path = '/'.$category->slug;

    }

    return $category_object;
}

function getContentForHomeTemplate($page, $lng){
    $object = new stdClass;
    $object->meta_title = $page->meta_title;
    $object->meta_description = $page->meta_description;
    $object->meta_image = $page->meta_image;
    $object->slug = $page->slug;
    $object->path = '/'.$page->slug;
    $object->template = $page->template;
    $object->title = $page->title;
    $object->intro_image = $page->intro_image;
    $object->intro_text = $page->intro_text;
    $object->favorites_heading = $page->favorites_heading;
    $object->favorites_more_label = $page->favorites_more_label;

    if($lng == 'lt'){
        $object->favorites = $page->favorites_lt;
    }
    elseif ($lng == 'ru'){
        $object->favorites = $page->favorites_ru;
    }
    else {
        $object->favorites = $page->favorites;
    }

    return $object;
}

function getContentForSimpleTemplate($page, $lng){
    $object = new stdClass;
    $object->meta_title = $page->meta_title;
    $object->meta_description = $page->meta_description;
    $object->meta_image = $page->meta_image;
    $object->slug = $page->slug;
    $object->path = '/'.$page->slug;
    $object->template = $page->template;
    $object->title = $page->title;
    $object->request = $page->request;

    if($lng == 'lt'){
        $object->sections = $page->section_simple_template_lt;
    }
    elseif ($lng == 'ru'){
        $object->sections = $page->section_simple_template_ru;
    }
    else{
        $object->sections = $page->section_simple_template;
    }

    return $object;
}

function getContentForCareerTemplate($page, $lng){

    $page_slug = $page->slug;

    $object = new stdClass;
    $object->meta_title = $page->meta_title;
    $object->meta_description = $page->meta_description;
    $object->meta_image = $page->meta_image;
    $object->slug = $page->slug;
    $object->path = '/'.$page->slug;
    $object->template = $page->template;
    $object->title = $page->title;
    $object->text = $page->career_text;

    $positions = Career::orderBy('sort_order', 'desc')->get();

    $positions_object = new stdClass;
    foreach ($positions as $position){
        $position_object = new stdClass;
        $position_object->meta_title = $position->meta_title;
        $position_object->meta_description = $position->meta_description;
        $position_object->meta_image = $position->meta_description;
        $position_object->title = $position->title;
        $position_object->slug = $position->slug;
        $position_object->path = '/'.$page_slug.'/'.$position->slug;
        $position_object->template = $position->template;
        $position_object->location = $position->location;
        $position_object->text = $position->text;
        $position_object->footer = $position->footer;
        if($lng == 'lt'){
            $position_object->list = $position->list_lt;
        }
        elseif ($lng == 'ru'){
            $position_object->list = $position->list_ru;
        }
        else{
            $position_object->list = $position->list;
        }

        $positions_slug = $position->slug;
        $positions_object->$positions_slug = $position_object;
    }

    $object->positions = $positions_object;

    return $object;
}

function getContentForContactsTemplate($page, $lng){
    $object = new stdClass;
    $object->meta_title = $page->meta_title;
    $object->meta_description = $page->meta_description;
    $object->meta_image = $page->meta_image;
    $object->slug = $page->slug;
    $object->path = '/'.$page->slug;
    $object->template = $page->template;
    $object->title = $page->title;
    if($lng == 'lt'){
        $blocks = getContactsBlocks($page->blocks_lt);
        $object->blocks = $blocks;
    }
    elseif($lng == 'ru'){
        $blocks = getContactsBlocks($page->blocks_ru);
        $object->blocks = $blocks;
    }
    else{
        $blocks = getContactsBlocks($page->blocks);
        $object->blocks = $blocks;
    }

    return $object;
}

function getContactsBlocks($blocks){
    $blocks_array = array();
    foreach ($blocks as $block){
        $single_block_object = new stdClass;
        $single_block_object->title = $block['title_repeater'];
        $single_block_object->info = $block['info'];
        $single_block_object->image = $block['image'];
        $single_block_object->color = $block['color'];
        $single_block_object->mapLat = $block['map_lat'];
        $single_block_object->mapLng = $block['map_long'];
        $single_block_object->marUrl = $block['map_url'];

        $branch_array = array();
        if(isset($block['branches'])){
            foreach ($block['branches'] as $branch){
                $single_branch = new stdClass;
                $single_branch->title = $branch['title_repeater'];
                $single_branch->fax = $branch['fax'];
                $single_branch->email = $branch['email'];
                if($branch['phones']){
                    $phones_array = explode(",", $branch['phones']);
                    $single_branch->phones = $phones_array;
                }
                array_push($branch_array, $single_branch);
            }
        }
        $single_block_object->branches = $branch_array;

        array_push($blocks_array, $single_block_object);
    }

    return $blocks_array;
}

function getContentForPageWithSubpages($page, $lng){
    $object = new stdClass;
    $object->meta_title = $page->meta_title;
    $object->meta_description = $page->meta_description;
    $object->meta_image = $page->meta_image;
    $object->slug = $page->slug;
    $object->path = '/'.$page->slug;
    $object->template = $page->template;
    $object->title = $page->title;

    $subpages = Subpage::where('page_id', $page->id)->get();

    foreach ($subpages as $subpage){
        $subpage->lang($lng);
        if($lng == 'lt' && $subpage->template == 'AboutTemplate'){
            $subpage->sections = $subpage->sections_lt;
        }
        elseif($lng == 'ru' && $subpage->template == 'AboutTemplate'){
            $subpage->sections = $subpage->sections_ru;
        }
        elseif($lng == 'lt' && $subpage->template == 'SimpleTemplate'){
            $subpage->sections = $subpage->simple_template_sections_lt;
        }
        elseif($lng == 'ru' && $subpage->template == 'SimpleTemplate'){
            $subpage->sections = $subpage->simple_template_sections_ru;
        }
        elseif($lng == 'en' && $subpage->template == 'SimpleTemplate'){
            $subpage->sections = $subpage->simple_template_sections;
        }

        unset($subpage->sections_lt);
        unset($subpage->sections_ru);
        unset($subpage->simple_template_sections_lt);
        unset($subpage->simple_template_sections_ru);
        unset($subpage->simple_template_sections);
    }

    $object->subpages = $subpages;

    return $object;
}

function getNav($lng){
    $object = new stdClass();
    $navigation_items = Navigation::get();
    $beverages_items = Beverage::get();

    foreach ($navigation_items as $item){
        $item->lang($lng);
        $slug = '';
        if($item->type == 'Page'){
            $path = Page::where('id', $item->page_id)->first()->lang($lng);
            $slug = '/'.$path->slug;
        }
        elseif ($item->type == 'Subpage'){
            $path = Subpage::where('id', $item->subpage_id)->first()->lang($lng);
            $parent_page_id = $path->page_id;
            $parent_path = Page::where('id', $parent_page_id)->first()->lang($lng);
            $slug = '/'.$parent_path->slug.'/'.$path->slug;
        }

        $item->path = $slug;
    }

    foreach ($beverages_items as $item){
        $item->lang($lng);
        $path = Category::where('id', $item->category_id)->first()->lang($lng);
        $item->path = '/'.$path->slug;
    }

    $object->items = $navigation_items;
    $object->beverages = $beverages_items;

    return $object;
}

function getMobNav($lng){
    $mobile_items = Mobile::get();

    foreach ($mobile_items as $item){
        $item->lang($lng);
        if($item->type == 'Page'){
            $page_id = $item->page_id;
            $page = Page::where('id', $page_id)->first()->lang($lng);
            $item->path = '/'.$page->slug;
        }
        elseif ($item->type == 'Submenu'){
            if($item->children){
                $childrens_array = array();
                foreach ($item->children as $children){
                    $obj = new stdClass();
                    if($children['type'] == 'Subpage'){
                        $subpage_id = $children['subpage'];
                        $subpage = Subpage::where('id', $subpage_id)->first()->lang($lng);
                        if($subpage){
                            $parent_page = Page::where('id', $subpage->page_id)->first()->lang($lng);
                            if($parent_page){
                                $obj->title = $subpage->title;
                                $obj->path = '/'.$parent_page->slug.'/'.$subpage->slug;
                                array_push($childrens_array,$obj);
                            }
                        }
                    }
                    elseif($children['type'] == 'Beverage'){
                        $product_category_id = $children['category'];
                        $category = Category::where('id', $product_category_id)->first()->lang($lng);
                        if($category){
                            $obj->title = $category->title;
                            $obj->path = '/'.$category->slug;
                            array_push($childrens_array,$obj);
                        }
                    }
                }
                $item->children = $childrens_array;
            }
        }

    }

    return $mobile_items;
}

function getNews($lng){
    $news = News::orderBy('date', 'desc')->get();
    foreach ($news as $new) {
        $new->lang($lng);
        if($lng == 'en'){
            $new->files = $new->files_en;
        }
        else if($lng == 'ru'){
            $new->files = $new->files_ru;
        }

        unset($new->files_ru);
        unset($new->files_en);

    }
    return $news;
}

function printJson($object){
    $json = json_encode($object);
    return $json;
}

Route::post('/endpoint/request', function(){
    if(isset($_POST['name'], $_POST['email'], $_POST['message'])){
        if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message'])){

            $company = '';
            $phone = '';
            if(isset($_POST['company']) && !empty($_POST['company'])){
                $company = $_POST['company'];
            }

            if(isset($_POST['phone']) && !empty($_POST['phone'])){
                $phone = $_POST['phone'];
            }

            $ui = Ui::first();
            $address_to_from_ui = $ui->request_email;

            $vars = [
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'text' => $_POST['message'],
                'to' => $address_to_from_ui,
                'company' => $company,
                'phone' => $phone
            ];

            Mail::send('newcode.routes::mail-templates.request', $vars, function($message) use ($vars) {

                $address_from = 'noreply@degtine.lt';
                $address_to = $vars['to'];
                $sender = $vars['email'];

                $message->from($address_from, $name = null);
                $message->replyTo($sender, $name = null);
                $message->sender($sender, $name = null);
                $message->to($address_to, $name = null);
                $message->subject('degtine.lt | message from: '.$vars['name']);

            });

            if(count(Mail::failures()) > 0){
                return "error";
            }
            else{
                return "success";
            }
        }
        else{
            return "error";
        }
    }
    else{
        return "error";
    }
});