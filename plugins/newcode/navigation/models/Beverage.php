<?php namespace Newcode\Navigation\Models;

use Model;

/**
 * Model
 */
class Beverage extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_navigation_beverages';

    public $belongsTo = [
        'category' => [
            'Newcode\Products\Models\Category',
            'table' => 'newcode_products_categories',
            'order' => 'title'
        ],
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
    ];
}
