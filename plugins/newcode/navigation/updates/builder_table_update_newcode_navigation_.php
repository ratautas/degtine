<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigation extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_', function($table)
        {
            $table->string('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
