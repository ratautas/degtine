<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeNavigationBeverages extends Migration
{
    public function up()
    {
        Schema::create('newcode_navigation_beverages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('color')->nullable();
            $table->integer('category_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_navigation_beverages');
    }
}
