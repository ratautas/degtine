<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigation2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_', function($table)
        {
            $table->string('type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_', function($table)
        {
            $table->dropColumn('type');
        });
    }
}
