<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationMobile extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->string('sort_order')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('title')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->dropColumn('sort_order');
            $table->increments('id')->unsigned()->change();
            $table->string('title', 191)->change();
        });
    }
}
