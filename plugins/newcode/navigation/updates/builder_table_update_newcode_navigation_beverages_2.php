<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationBeverages2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_beverages', function($table)
        {
            $table->string('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_beverages', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
