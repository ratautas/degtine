<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeNavigation extends Migration
{
    public function up()
    {
        Schema::create('newcode_navigation_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('page_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_navigation_');
    }
}
