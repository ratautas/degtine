<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationMobile5 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->integer('page_id')->nullable();
            $table->string('type')->nullable();
            $table->dropColumn('slug');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->dropColumn('page_id');
            $table->dropColumn('type');
            $table->string('slug', 191)->nullable();
        });
    }
}
