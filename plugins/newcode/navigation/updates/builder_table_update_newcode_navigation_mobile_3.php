<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationMobile3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->string('slug')->nullable();
            $table->string('type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->dropColumn('slug');
            $table->dropColumn('type');
        });
    }
}
