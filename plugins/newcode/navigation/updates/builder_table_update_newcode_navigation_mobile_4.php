<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationMobile4 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->string('slug')->change();
            $table->dropColumn('type');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->string('slug', 191)->change();
            $table->string('type', 191)->nullable();
        });
    }
}
