<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigation3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_', function($table)
        {
            $table->integer('subpage_id')->nullable();
            $table->string('type')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_', function($table)
        {
            $table->dropColumn('subpage_id');
            $table->string('type', 191)->change();
        });
    }
}
