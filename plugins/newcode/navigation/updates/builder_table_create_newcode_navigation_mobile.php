<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeNavigationMobile extends Migration
{
    public function up()
    {
        Schema::create('newcode_navigation_mobile', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('page_id')->nullable();
            $table->boolean('sub_menu')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_navigation_mobile');
    }
}
