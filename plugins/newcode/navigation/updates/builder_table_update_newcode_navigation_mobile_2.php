<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationMobile2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->text('children')->nullable();
            $table->string('sort_order')->change();
            $table->dropColumn('page_id');
            $table->dropColumn('sub_menu');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->dropColumn('children');
            $table->string('sort_order', 191)->change();
            $table->integer('page_id')->nullable();
            $table->boolean('sub_menu')->nullable()->default(0);
        });
    }
}
