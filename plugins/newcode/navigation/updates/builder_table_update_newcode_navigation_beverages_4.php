<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationBeverages4 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_beverages', function($table)
        {
            $table->dropColumn('page_id');
            $table->dropColumn('type');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_beverages', function($table)
        {
            $table->integer('page_id')->nullable();
            $table->string('type', 191)->nullable();
        });
    }
}
