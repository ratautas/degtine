<?php namespace Newcode\Navigation\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNavigationMobile6 extends Migration
{
    public function up()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->boolean('show_lt')->nullable()->default(0);
            $table->boolean('show_en')->nullable()->default(0);
            $table->boolean('show_ru')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('newcode_navigation_mobile', function($table)
        {
            $table->dropColumn('show_lt');
            $table->dropColumn('show_en');
            $table->dropColumn('show_ru');
        });
    }
}
