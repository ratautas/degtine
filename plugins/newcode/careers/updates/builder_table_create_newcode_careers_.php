<?php namespace Newcode\Careers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeCareers extends Migration
{
    public function up()
    {
        Schema::create('newcode_careers_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            $table->string('location')->nullable();
            $table->string('text')->nullable();
            $table->text('list')->nullable();
            $table->string('footer')->nullable();
            $table->string('slug')->nullable();
            $table->string('path')->nullable();
            $table->string('template')->nullable()->default('PositionTemplate');
            $table->string('title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_careers_');
    }
}
