<?php namespace Newcode\Careers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeCareers extends Migration
{
    public function up()
    {
        Schema::table('newcode_careers_', function($table)
        {
            $table->text('list_lt')->nullable();
            $table->text('lang')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('meta_title')->change();
            $table->string('meta_description')->change();
            $table->string('meta_image')->change();
            $table->string('location')->change();
            $table->text('text')->nullable()->unsigned(false)->default(null)->change();
            $table->string('footer')->change();
            $table->string('slug')->change();
            $table->string('path')->change();
            $table->string('template')->change();
            $table->string('title')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_careers_', function($table)
        {
            $table->dropColumn('list_lt');
            $table->dropColumn('lang');
            $table->increments('id')->unsigned()->change();
            $table->string('meta_title', 191)->change();
            $table->string('meta_description', 191)->change();
            $table->string('meta_image', 191)->change();
            $table->string('location', 191)->change();
            $table->string('text', 191)->nullable()->unsigned(false)->default(null)->change();
            $table->string('footer', 191)->change();
            $table->string('slug', 191)->change();
            $table->string('path', 191)->change();
            $table->string('template', 191)->change();
            $table->string('title', 191)->change();
        });
    }
}
