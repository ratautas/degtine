<?php namespace Newcode\Careers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeCareers3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_careers_', function($table)
        {
            $table->text('list_ru')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_careers_', function($table)
        {
            $table->dropColumn('list_ru');
        });
    }
}
