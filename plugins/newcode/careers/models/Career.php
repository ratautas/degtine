<?php namespace Newcode\Careers\Models;

use Model;

/**
 * Model
 */
class Career extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_careers_';

    protected  $jsonable = ['list', 'list_lt', 'list_ru'];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'meta_title',
        'meta_description',
        'title',
        ['slug', 'index' => true],
        'text',
        'location',
        'footer'
    ];
}
