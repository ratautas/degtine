<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts7 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('image');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->string('image', 191)->nullable();
        });
    }
}
