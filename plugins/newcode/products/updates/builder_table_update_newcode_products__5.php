<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts5 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('type');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->string('type', 191)->nullable();
        });
    }
}
