<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeProductsCategoriesProducts extends Migration
{
    public function up()
    {
        Schema::create('newcode_products_categories_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id')->nullable();
            $table->integer('product_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_products_categories_products');
    }
}
