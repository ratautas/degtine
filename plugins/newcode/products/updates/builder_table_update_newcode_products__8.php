<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts8 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->string('right_section_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('right_section_title');
        });
    }
}
