<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->text('left_section')->nullable();
            $table->text('right_section')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('left_section');
            $table->dropColumn('right_section');
        });
    }
}
