<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts13 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->renameColumn('show', 'show_lt');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->renameColumn('show_lt', 'show');
        });
    }
}
