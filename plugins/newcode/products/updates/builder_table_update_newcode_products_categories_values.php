<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProductsCategoriesValues extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_categories_values', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_categories_values', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
