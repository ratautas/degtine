<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts14 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->string('subcategory')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('subcategory');
        });
    }
}
