<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts6 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->text('left_section_en')->nullable();
            $table->text('right_section_en')->nullable();
            $table->text('left_section_ru')->nullable();
            $table->text('right_section_ru')->nullable();
            $table->string('lng')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('left_section_en');
            $table->dropColumn('right_section_en');
            $table->dropColumn('left_section_ru');
            $table->dropColumn('right_section_ru');
            $table->dropColumn('lng');
        });
    }
}
