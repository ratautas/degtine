<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeProductsFilters extends Migration
{
    public function up()
    {
        Schema::create('newcode_products_filters', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('label')->nullable();
            $table->integer('reset')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_products_filters');
    }
}
