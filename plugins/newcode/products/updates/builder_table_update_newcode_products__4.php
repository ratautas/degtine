<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts4 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->text('filters')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('filters');
        });
    }
}
