<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeProducts extends Migration
{
    public function up()
    {
        Schema::create('newcode_products_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('template')->nullable()->default('BeverageTemplate');
            $table->string('type')->nullable();
            $table->string('slug')->nullable();
            $table->string('image')->nullable();
            $table->string('color')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_products_');
    }
}
