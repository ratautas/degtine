<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts10 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->renameColumn('sizes_lt', 'sizes');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->renameColumn('sizes', 'sizes_lt');
        });
    }
}
