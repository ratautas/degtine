<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts16 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->boolean('hide_everything')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('hide_everything');
        });
    }
}
