<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeProductsCategories extends Migration
{
    public function up()
    {
        Schema::create('newcode_products_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('template')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            $table->string('color')->nullable();
            $table->text('filters')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_products_categories');
    }
}
