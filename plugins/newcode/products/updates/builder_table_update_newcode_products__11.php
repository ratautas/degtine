<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts11 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->boolean('show')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('show');
        });
    }
}
