<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeProductsFilterValues extends Migration
{
    public function up()
    {
        Schema::create('newcode_products_filter_values', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('label')->nullable();
            $table->string('value')->nullable();
            $table->integer('filter_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_products_filter_values');
    }
}
