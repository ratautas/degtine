<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProductsFilters extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_filters', function($table)
        {
            $table->string('title')->nullable();
            $table->increments('id')->unsigned(false)->change();
            $table->string('label')->nullable()->unsigned(false)->default(null)->change();
            $table->string('reset')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_filters', function($table)
        {
            $table->dropColumn('title');
            $table->increments('id')->unsigned()->change();
            $table->integer('label')->nullable()->unsigned(false)->default(null)->change();
            $table->integer('reset')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
