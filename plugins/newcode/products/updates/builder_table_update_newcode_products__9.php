<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts9 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->text('sizes_lt')->nullable();
            $table->text('sizes_en')->nullable();
            $table->text('sizes_ru')->nullable();
            $table->dropColumn('right_section_title');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('sizes_lt');
            $table->dropColumn('sizes_en');
            $table->dropColumn('sizes_ru');
            $table->string('right_section_title', 191)->nullable();
        });
    }
}
