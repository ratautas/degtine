<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProductsCategories extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_categories', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('title')->change();
            $table->string('slug')->change();
            $table->string('template')->default('BeverageCategory')->change();
            $table->string('meta_title')->change();
            $table->string('meta_description')->change();
            $table->string('meta_image')->change();
            $table->string('color')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_categories', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('title', 191)->change();
            $table->string('slug', 191)->change();
            $table->string('template', 191)->default(null)->change();
            $table->string('meta_title', 191)->change();
            $table->string('meta_description', 191)->change();
            $table->string('meta_image', 191)->change();
            $table->string('color', 191)->change();
        });
    }
}
