<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProducts12 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->boolean('show_en')->default(0);
            $table->boolean('show_ru')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_', function($table)
        {
            $table->dropColumn('show_en');
            $table->dropColumn('show_ru');
        });
    }
}
