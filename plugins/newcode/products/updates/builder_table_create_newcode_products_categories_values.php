<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeProductsCategoriesValues extends Migration
{
    public function up()
    {
        Schema::create('newcode_products_categories_values', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('category_id')->nullable();
            $table->integer('value_id')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_products_categories_values');
    }
}
