<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProductsCategories2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_categories', function($table)
        {
            $table->dropColumn('filters');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_categories', function($table)
        {
            $table->text('filters')->nullable();
        });
    }
}
