<?php namespace Newcode\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeProductsCategories3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_products_categories', function($table)
        {
            $table->integer('sord_order');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_products_categories', function($table)
        {
            $table->dropColumn('sord_order');
        });
    }
}
