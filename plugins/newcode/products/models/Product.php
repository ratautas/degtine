<?php namespace Newcode\Products\Models;

use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_products_';

    protected $jsonable = ['left_section', 'right_section', 'filters', 'left_section_ru', 'left_section_en', 'right_section_ru', 'right_section_en', 'sizes', 'sizes_ru', 'sizes_en'];

    public $belongsTo = [
        'filter' => [
            'Newcode\Products\Models\Filter',
            'table' => 'newcode_products_filters',
            'order' => 'title'
        ],
        'value' => [
            'Newcode\Products\Models\Value',
            'table' => 'newcode_products_values',
            'order' => 'label'
        ],
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'meta_title',
        'meta_description',
        'title',
        ['slug', 'index' => true],
        'description',
        'right_section_title',
        'subcategory'
    ];
}
