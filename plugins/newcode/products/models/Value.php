<?php namespace Newcode\Products\Models;

use Model;

/**
 * Model
 */
class Value extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_products_filter_values';

    public $belongsTo = [
        'filter' => [
            'Newcode\Products\Models\Filter',
            'table' => 'newcode_products_filters',
            'order' => 'title'
        ],
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'label',
        'value',
    ];
}
