<?php namespace Newcode\Products\Models;

use Model;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_products_categories';

    public $belongsToMany = [
        'filters' => [
            'Newcode\Products\Models\Filter',
            'table'    => 'newcode_products_categories_filters',
            'key'      => 'category_id',
            'otherKey' => 'filter_id',
        ],
        'products' => [
            'Newcode\Products\Models\Product',
            'table'    => 'newcode_products_categories_products',
            'key'      => 'category_id',
            'otherKey' => 'product_id'
        ],
        'values' => [
            'Newcode\Products\Models\Value',
            'table'    => 'newcode_products_categories_values',
            'key'      => 'category_id',
            'otherKey' => 'value_id'
        ]
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'meta_title',
        'meta_description',
        'title',
        ['slug', 'index' => true],
    ];

    public $hasMany = [
        'Beverage' => [
            'Newcode\Navigation\Models\Beverage',
            'table'    => 'newcode_navigation_beverages',
            'delete' => true
        ]
    ];
}
