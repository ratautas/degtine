<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi4 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('filter_labels_country', 191)->nullable();
            $table->string('filter_labels_brand')->nullable();
            $table->string('filter_labels_type')->nullable();
            $table->string('filter_resets_country')->nullable();
            $table->string('filter_resets_brand')->nullable();
            $table->string('filter_resets_type')->nullable();
            $table->string('copyright')->change();
            $table->string('created_by')->change();
            $table->dropColumn('filter_labels');
            $table->dropColumn('filter_resets');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('filter_labels_country');
            $table->dropColumn('filter_labels_brand');
            $table->dropColumn('filter_labels_type');
            $table->dropColumn('filter_resets_country');
            $table->dropColumn('filter_resets_brand');
            $table->dropColumn('filter_resets_type');
            $table->string('copyright', 191)->change();
            $table->string('created_by', 191)->change();
            $table->string('filter_labels', 191)->nullable();
            $table->string('filter_resets', 191)->nullable();
        });
    }
}
