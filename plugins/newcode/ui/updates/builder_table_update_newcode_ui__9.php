<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi9 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('gate_reject_text')->nullable();
            $table->string('return_home')->change();
            $table->string('not_found')->change();
            $table->string('no_results')->change();
            $table->string('cookies_text')->change();
            $table->string('cookies_notice')->change();
            $table->string('cookies_agree')->change();
            $table->string('cookies_slug')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('gate_reject_text');
            $table->string('return_home', 191)->change();
            $table->string('not_found', 191)->change();
            $table->string('no_results', 191)->change();
            $table->string('cookies_text', 191)->change();
            $table->string('cookies_notice', 191)->change();
            $table->string('cookies_agree', 191)->change();
            $table->string('cookies_slug', 191)->change();
        });
    }
}
