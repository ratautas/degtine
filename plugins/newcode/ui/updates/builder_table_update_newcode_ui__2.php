<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('phones_label')->nullable();
            $table->string('fax_label')->nullable();
            $table->string('mobile_label')->nullable();
            $table->string('email_label')->nullable();
            $table->string('related_label')->nullable();
            $table->string('nav_open_label')->nullable();
            $table->string('nav_close_label')->nullable();
            $table->string('nav_back_label')->nullable();
            $table->string('gate_heading')->change();
            $table->string('gate_agree')->change();
            $table->string('gate_reject')->change();
            $table->string('gate_footer')->change();
            $table->string('back')->change();
            $table->string('view_map')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('phones_label');
            $table->dropColumn('fax_label');
            $table->dropColumn('mobile_label');
            $table->dropColumn('email_label');
            $table->dropColumn('related_label');
            $table->dropColumn('nav_open_label');
            $table->dropColumn('nav_close_label');
            $table->dropColumn('nav_back_label');
            $table->string('gate_heading', 191)->change();
            $table->string('gate_agree', 191)->change();
            $table->string('gate_reject', 191)->change();
            $table->string('gate_footer', 191)->change();
            $table->string('back', 191)->change();
            $table->string('view_map', 191)->change();
        });
    }
}
