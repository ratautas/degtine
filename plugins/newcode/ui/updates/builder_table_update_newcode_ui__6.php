<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi6 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('error_name')->nullable();
            $table->string('error_email')->nullable();
            $table->string('error_phone')->nullable();
            $table->string('error_global')->nullable();
            $table->string('filter_label')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('error_name');
            $table->dropColumn('error_email');
            $table->dropColumn('error_phone');
            $table->dropColumn('error_global');
            $table->dropColumn('filter_label');
        });
    }
}
