<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeUi extends Migration
{
    public function up()
    {
        Schema::create('newcode_ui_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('default_meta_title')->nullable();
            $table->string('default_meta_description')->nullable();
            $table->string('default_meta_image')->nullable();
            $table->string('page_suffix')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_ui_');
    }
}
