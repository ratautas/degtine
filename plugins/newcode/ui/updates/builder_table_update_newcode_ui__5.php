<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi5 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('request_image')->nullable();
            $table->string('request_name')->nullable();
            $table->string('request_email')->nullable();
            $table->string('request_phone')->nullable();
            $table->string('request_name_label')->nullable();
            $table->string('request_company_label')->nullable();
            $table->string('request_email_label')->nullable();
            $table->string('request_phone_label')->nullable();
            $table->string('request_message_label')->nullable();
            $table->string('request_submit_label')->nullable();
            $table->dropColumn('filter_labels_country');
            $table->dropColumn('filter_labels_brand');
            $table->dropColumn('filter_labels_type');
            $table->dropColumn('filter_resets_country');
            $table->dropColumn('filter_resets_brand');
            $table->dropColumn('filter_resets_type');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('request_image');
            $table->dropColumn('request_name');
            $table->dropColumn('request_email');
            $table->dropColumn('request_phone');
            $table->dropColumn('request_name_label');
            $table->dropColumn('request_company_label');
            $table->dropColumn('request_email_label');
            $table->dropColumn('request_phone_label');
            $table->dropColumn('request_message_label');
            $table->dropColumn('request_submit_label');
            $table->string('filter_labels_country', 191)->nullable();
            $table->string('filter_labels_brand', 191)->nullable();
            $table->string('filter_labels_type', 191)->nullable();
            $table->string('filter_resets_country', 191)->nullable();
            $table->string('filter_resets_brand', 191)->nullable();
            $table->string('filter_resets_type', 191)->nullable();
        });
    }
}
