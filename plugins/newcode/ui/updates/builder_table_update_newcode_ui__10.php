<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi10 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('read_more_label')->nullable();
            $table->string('size_label')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('read_more_label');
            $table->dropColumn('size_label');
        });
    }
}
