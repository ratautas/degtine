<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('gate_heading')->nullable();
            $table->string('gate_agree')->nullable();
            $table->string('gate_reject')->nullable();
            $table->string('gate_footer')->nullable();
            $table->string('back')->nullable();
            $table->string('view_map')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('gate_heading');
            $table->dropColumn('gate_agree');
            $table->dropColumn('gate_reject');
            $table->dropColumn('gate_footer');
            $table->dropColumn('back');
            $table->dropColumn('view_map');
        });
    }
}
