<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('filter_labels')->nullable();
            $table->string('filter_resets')->nullable();
            $table->string('copyright')->nullable();
            $table->string('created_by')->nullable();
            $table->string('gate_heading')->change();
            $table->string('gate_agree')->change();
            $table->string('gate_reject')->change();
            $table->string('gate_footer')->change();
            $table->string('back')->change();
            $table->string('view_map')->change();
            $table->string('phones_label')->change();
            $table->string('fax_label')->change();
            $table->string('mobile_label')->change();
            $table->string('email_label')->change();
            $table->string('related_label')->change();
            $table->string('nav_open_label')->change();
            $table->string('nav_close_label')->change();
            $table->string('nav_back_label')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('filter_labels');
            $table->dropColumn('filter_resets');
            $table->dropColumn('copyright');
            $table->dropColumn('created_by');
            $table->string('gate_heading', 191)->change();
            $table->string('gate_agree', 191)->change();
            $table->string('gate_reject', 191)->change();
            $table->string('gate_footer', 191)->change();
            $table->string('back', 191)->change();
            $table->string('view_map', 191)->change();
            $table->string('phones_label', 191)->change();
            $table->string('fax_label', 191)->change();
            $table->string('mobile_label', 191)->change();
            $table->string('email_label', 191)->change();
            $table->string('related_label', 191)->change();
            $table->string('nav_open_label', 191)->change();
            $table->string('nav_close_label', 191)->change();
            $table->string('nav_back_label', 191)->change();
        });
    }
}
