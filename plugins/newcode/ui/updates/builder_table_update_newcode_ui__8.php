<?php namespace Newcode\Ui\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeUi8 extends Migration
{
    public function up()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->string('return_home')->nullable();
            $table->string('not_found')->nullable();
            $table->string('no_results')->nullable();
            $table->string('cookies_text')->nullable();
            $table->string('cookies_notice')->nullable();
            $table->string('cookies_agree')->nullable();
            $table->string('cookies_slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_ui_', function($table)
        {
            $table->dropColumn('return_home');
            $table->dropColumn('not_found');
            $table->dropColumn('no_results');
            $table->dropColumn('cookies_text');
            $table->dropColumn('cookies_notice');
            $table->dropColumn('cookies_agree');
            $table->dropColumn('cookies_slug');
        });
    }
}
