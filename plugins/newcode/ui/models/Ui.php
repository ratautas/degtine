<?php namespace Newcode\Ui\Models;

use Model;

/**
 * Model
 */
class Ui extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_ui_';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'default_meta_title',
        'default_meta_description',
        'default_meta_image',
        'page_suffix',
        'gate_heading',
        'gate_agree',
        'gate_reject',
        'gate_footer',
        'back',
        'view_map',
        'phones_label',
        'fax_label',
        'mobile_label',
        'email_label',
        'related_label',
        'nav_open_label',
        'nav_close_label',
        'nav_back_label',
        'copyright',
        'created_by',
        'request_name',
        'request_email',
        'request_phone',
        'request_name_label',
        'request_company_label',
        'request_email_label',
        'request_phone_label',
        'request_message_label',
        'request_submit_label',
        'filter_label',
        'error_name',
        'error_email',
        'error_phone',
        'error_global',
        'gate_reject_text',
        'cookies_slug',
        'cookies_agree',
        'cookies_notice',
        'cookies_text',
        'no_results',
        'not_found',
        'return_home',
        'size_label',
        'read_more_label'
    ];
}
