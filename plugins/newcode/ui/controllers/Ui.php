<?php namespace Newcode\Ui\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Ui extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Newcode.Ui', 'main-menu-item');
    }
}
