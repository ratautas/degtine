<?php namespace Newcode\Pages\Models;

use Model;

/**
 * Model
 */
class Subpage extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;


    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_pages_subpages';

    protected $jsonable = ['sections_lt', 'sections', 'sections_ru', 'simple_template_sections_ru', 'simple_template_sections_lt', 'simple_template_sections'];

    public $belongsTo = [
        'page' => [
            'Newcode\Pages\Models\Page',
            'table' => 'newcode_pages_',
            'order' => 'title'
        ],
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'meta_title',
        'meta_description',
        'title',
        ['slug', 'index' => true],
        'nav_title'
    ];

    public $hasMany = [
        'Navigation' => ['Newcode\Navigation\Models\Navigation', 'delete' => true],
        //'Mobile' => ['Newcode\Navigation\Models\Mobile', 'delete' => true]
    ];
}
