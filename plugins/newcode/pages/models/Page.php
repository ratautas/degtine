<?php namespace Newcode\Pages\Models;

use Model;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_pages_';

    protected $jsonable = [
        'favorites',
        'favorites_lt',
        'favorites_ru',
        'section_simple_template',
        'section_simple_template_lt',
        'section_simple_template_ru',
        'blocks_lt',
        'blocks',
        'blocks_ru'
    ];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'meta_title',
        'meta_description',
        'title',
        ['slug', 'index' => true],
        'intro_text',
        'favorites_heading',
        'favorites_more_label'
    ];

    public $hasMany = [
        'Navigation' => ['Newcode\Navigation\Models\Navigation', 'delete' => true],
        'Mobile' => ['Newcode\Navigation\Models\Mobile', 'delete' => true],
        'Subpage' => ['Newcode\Pages\Models\Subpage', 'delete' => true]
    ];
}
