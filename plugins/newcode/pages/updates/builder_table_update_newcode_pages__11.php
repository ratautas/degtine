<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages11 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->text('blocks')->nullable();
            $table->text('blocks_lt')->nullable();
            $table->text('blocks_ru')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->dropColumn('blocks');
            $table->dropColumn('blocks_lt');
            $table->dropColumn('blocks_ru');
        });
    }
}
