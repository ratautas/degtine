<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodePagesSubpages extends Migration
{
    public function up()
    {
        Schema::create('newcode_pages_subpages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            $table->string('title')->nullable();
            $table->string('nav_title')->nullable();
            $table->string('slug')->nullable();
            $table->string('template')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_pages_subpages');
    }
}
