<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages10 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->text('favorites_ru')->nullable();
            $table->text('section_simple_template_ru')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->dropColumn('favorites_ru');
            $table->dropColumn('section_simple_template_ru');
        });
    }
}
