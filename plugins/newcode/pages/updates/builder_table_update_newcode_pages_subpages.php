<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePagesSubpages extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->integer('page_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->dropColumn('page_id');
        });
    }
}
