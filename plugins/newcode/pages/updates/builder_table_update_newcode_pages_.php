<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('meta_description')->change();
            $table->string('meta_image')->change();
            $table->string('slug')->change();
            $table->string('template')->change();
            $table->string('title')->change();
            $table->dropColumn('path');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('meta_description', 191)->change();
            $table->string('meta_image', 191)->change();
            $table->string('slug', 191)->change();
            $table->string('template', 191)->change();
            $table->string('title', 191)->change();
            $table->string('path', 191)->nullable();
        });
    }
}
