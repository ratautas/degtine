<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->string('lng')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->dropColumn('lng');
        });
    }
}
