<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePagesSubpages2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->string('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
