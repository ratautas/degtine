<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages14 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->boolean('show_lt')->nullable()->unsigned(false)->default(0)->change();
            $table->boolean('show_en')->nullable()->unsigned(false)->default(0)->change();
            $table->boolean('show_ru')->nullable()->unsigned(false)->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->string('show_lt', 191)->nullable()->unsigned(false)->default('0')->change();
            $table->string('show_en', 191)->nullable()->unsigned(false)->default('0')->change();
            $table->string('show_ru', 191)->nullable()->unsigned(false)->default('0')->change();
        });
    }
}
