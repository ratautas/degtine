<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->string('intro_image')->nullable();
            $table->string('intro_text')->nullable();
            $table->string('favorites_heading')->nullable();
            $table->string('favorites_more_label')->nullable();
            $table->string('meta_description')->change();
            $table->string('meta_image')->change();
            $table->string('slug')->change();
            $table->string('template')->change();
            $table->string('title')->change();
            $table->renameColumn('content', 'favorites');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->dropColumn('intro_image');
            $table->dropColumn('intro_text');
            $table->dropColumn('favorites_heading');
            $table->dropColumn('favorites_more_label');
            $table->string('meta_description', 191)->change();
            $table->string('meta_image', 191)->change();
            $table->string('slug', 191)->change();
            $table->string('template', 191)->change();
            $table->string('title', 191)->change();
            $table->renameColumn('favorites', 'content');
        });
    }
}
