<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodePages extends Migration
{
    public function up()
    {
        Schema::create('newcode_pages_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            $table->string('path')->nullable();
            $table->string('slug')->nullable();
            $table->string('template')->nullable();
            $table->string('title')->nullable();
            $table->text('content')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_pages_');
    }
}
