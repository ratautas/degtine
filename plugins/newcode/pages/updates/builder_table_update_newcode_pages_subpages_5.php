<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePagesSubpages5 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->text('simple_template_sections')->nullable();
            $table->text('simple_template_sections_ru')->nullable();
            $table->text('simple_template_sections_lt')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->dropColumn('simple_template_sections');
            $table->dropColumn('simple_template_sections_ru');
            $table->dropColumn('simple_template_sections_lt');
        });
    }
}
