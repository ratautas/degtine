<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePagesSubpages3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->string('featured_type')->nullable();
            $table->string('featured_image')->nullable();
            $table->text('sections')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->dropColumn('featured_type');
            $table->dropColumn('featured_image');
            $table->dropColumn('sections');
        });
    }
}
