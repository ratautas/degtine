<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePagesSubpages4 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->text('sections_lt')->nullable();
            $table->text('sections_ru')->nullable();
            $table->string('featured_type')->change();
            $table->string('featured_image')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->dropColumn('sections_lt');
            $table->dropColumn('sections_ru');
            $table->string('featured_type', 191)->change();
            $table->string('featured_image', 191)->change();
        });
    }
}
