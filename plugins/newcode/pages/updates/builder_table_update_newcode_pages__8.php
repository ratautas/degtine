<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePages8 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->string('meta_title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_', function($table)
        {
            $table->dropColumn('meta_title');
        });
    }
}
