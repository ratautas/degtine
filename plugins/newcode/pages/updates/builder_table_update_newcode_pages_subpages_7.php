<?php namespace Newcode\Pages\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodePagesSubpages7 extends Migration
{
    public function up()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->boolean('show_lt')->nullable()->default(0);
            $table->boolean('show_en')->nullable()->default(0);
            $table->boolean('show_ru')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('newcode_pages_subpages', function($table)
        {
            $table->dropColumn('show_lt');
            $table->dropColumn('show_en');
            $table->dropColumn('show_ru');
        });
    }
}
