<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews5 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->boolean('show_lt')->nullable()->default(0);
            $table->boolean('show_ru')->nullable()->default(0);
            $table->boolean('show_en')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->dropColumn('show_lt');
            $table->dropColumn('show_ru');
            $table->dropColumn('show_en');
        });
    }
}
