<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews4 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->dropColumn('date');
        });
    }
}
