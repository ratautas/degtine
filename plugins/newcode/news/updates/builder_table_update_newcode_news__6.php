<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews6 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('date', 191)->default('2001-01-1')->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('date', 191)->default(null)->change();
        });
    }
}
