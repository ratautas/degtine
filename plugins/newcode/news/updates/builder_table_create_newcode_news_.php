<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNewcodeNews extends Migration
{
    public function up()
    {
        Schema::create('newcode_news_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->text('content')->nullable();
            $table->text('content_inner')->nullable();
            $table->text('files')->nullable();
            $table->text('files_en')->nullable();
            $table->text('files_ru')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('newcode_news_');
    }
}
