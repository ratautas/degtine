<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews2 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('og_title')->change();
            $table->string('og_description')->change();
            $table->string('og_image')->change();
            $table->dropColumn('files_en');
            $table->dropColumn('files_ru');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('og_title', 191)->change();
            $table->string('og_description', 191)->change();
            $table->string('og_image', 191)->change();
            $table->text('files_en')->nullable();
            $table->text('files_ru')->nullable();
        });
    }
}
