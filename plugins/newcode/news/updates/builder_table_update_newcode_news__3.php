<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews3 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_image')->nullable();
            $table->dropColumn('og_title');
            $table->dropColumn('og_description');
            $table->dropColumn('og_image');
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_image');
            $table->string('og_title', 191)->nullable();
            $table->string('og_description', 191)->nullable();
            $table->string('og_image', 191)->nullable();
        });
    }
}
