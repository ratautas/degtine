<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews9 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->text('files_en')->nullable();
            $table->text('files_ru')->nullable();
            $table->string('lng')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->dropColumn('files_en');
            $table->dropColumn('files_ru');
            $table->dropColumn('lng');
        });
    }
}
