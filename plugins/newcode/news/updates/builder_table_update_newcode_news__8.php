<?php namespace Newcode\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNewcodeNews8 extends Migration
{
    public function up()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->dateTime('date')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('newcode_news_', function($table)
        {
            $table->string('date', 191)->nullable()->unsigned(false)->default('2001-01-01 15:15:15')->change();
        });
    }
}
