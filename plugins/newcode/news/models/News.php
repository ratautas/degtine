<?php namespace Newcode\News\Models;

use Model;

/**
 * Model
 */
class News extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'newcode_news_';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = [
        'title',
        ['slug', 'index' => true],
        'content',
        'content_inner',
        'meta_title',
        'meta_description'
    ];

    protected $jsonable = ['files', 'files_en', 'files_ru'];
}
