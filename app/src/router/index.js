import Vue from 'vue'
import Router from 'vue-router'

import store from '@/store'

import ifDefined from '@/tools/ifDefined'

import SearchResultsTemplate from '@/templates/SearchResultsTemplate'

Vue.use(Router)

var router = new Router({
  mode: 'history',
  linkActiveClass: 'is-active',
  linkExactActiveClass: 'is-exact',
  base: '/' + document.body.getAttribute('data-lang') + '/',
  routes: [{
    path: '/search/:q',
    name: 'SearchResults',
    component: SearchResultsTemplate
  }]
})

router.beforeEach((to, from, next) => {
  store.dispatch('setCurrentPage', to.meta)
  if (ifDefined(to.meta.subpages)) {
    router.push(to.path + '/' + to.meta.subpages[0]['slug'])
  }
  next()
})

router.afterEach((to, from) => {
  if (to.name) { store.dispatch('scrollUnFix') }
  store.dispatch('closeModal')
})

export default router
