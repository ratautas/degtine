var ua = window.navigator.userAgent
var OS = null

if (ua.indexOf('Windows NT 10.0') !== -1) OS = 'win-10'
if (ua.indexOf('Windows NT 6.2') !== -1) OS = 'win-8'
if (ua.indexOf('Windows NT 6.1') !== -1) OS = 'win-7'
if (ua.indexOf('Windows NT 5.1') !== -1) OS = 'win-xp'
if (ua.indexOf('Mac') !== -1) OS = 'mac-os'
if (ua.indexOf('Linux') !== -1) OS = 'linux'

export default {
  state: {
    device: {
      isPortrait: screen.width < screen.height,
      ratio: screen.width / screen.height,
      isTouch: typeof window.ontouchstart !== 'undefined',
      isMob: screen.width < 768,
      isTablet: screen.width < 1025 && screen.width > 767,
      isLaptop: screen.width > 1024,
      winW: window.innerWidth,
      winH: window.innerHeight,
      bodyW: document.body.clientWidth,
      bodyH: document.body.clientHeight,
      scrollbarW: window.innerWidth - document.body.clientWidth,
      hasScrollbar: document.body.clientWidth !== window.innerWidth,
      isIOS: ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0,
      isIE: /msie\s|trident\/|edge\//i.test(ua),
      OS: OS,
      ua: ua
    }
  },
  getters: {
    device: state => state.device
  },
  actions: {
    deviceCheck({ commit }, s) {
      commit('SET_DEVICE', {
        winW: window.innerWidth,
        winH: window.innerHeight,
        bodyW: document.body.clientWidth,
        bodyH: document.body.clientHeight,
        scrollbarW: window.innerWidth - document.body.clientWidth,
        hasScrollbar: document.body.clientWidth !== window.innerWidth
      })
    }
  },
  mutations: {
    SET_DEVICE(state, d) {
      state.device.winW = d.winW
      state.device.bodyW = d.bodyW
      state.device.bodyH = d.bodyH
      state.device.scrollbarW = d.scrollbarW
      state.device.hasScrollbar = d.hasScrollbar
    }
  }
}
