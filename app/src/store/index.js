import Vue from 'vue'
import Vuex from 'vuex'
import latinize from 'latinize'

import device from '@/store/device'

import router from '@/router'

import ifDefined from '@/tools/ifDefined'
import trim from '@/tools/trim'

import PageNotFound from '@/templates/PageNotFound'
import AboutTemplate from '@/templates/AboutTemplate'
import BeverageCategory from '@/templates/BeverageCategory'
import BeverageTemplate from '@/templates/BeverageTemplate'
import CareerTemplate from '@/templates/CareerTemplate'
import ContactsTemplate from '@/templates/ContactsTemplate'
import HomeTemplate from '@/templates/HomeTemplate'
import PositionTemplate from '@/templates/PositionTemplate'
import PostTemplate from '@/templates/PostTemplate'
import SimpleTemplate from '@/templates/SimpleTemplate'
import PageWithSubpages from '@/templates/PageWithSubpages'

Vue.use(Vuex)

var templates = {
  PageNotFound,
  AboutTemplate,
  BeverageCategory,
  BeverageTemplate,
  CareerTemplate,
  ContactsTemplate,
  HomeTemplate,
  PositionTemplate,
  SimpleTemplate,
  PostTemplate,
  PageWithSubpages
}

var dev = process.env.NODE_ENV !== 'production'

var baseURL = document.body.getAttribute('data-page-url')

var store = new Vuex.Store({
  state: {
    baseURL,
    lang: document.body.getAttribute('data-lang'),
    loading: true,
    gate: true,
    cookies: false,
    about: {},
    beverages: {},
    allBeverages: {},
    categories: {},
    news: {},
    filters: {},
    filterLabelMap: {},
    filterValueMap: {},
    positions: {},
    pages: [],
    ui: {},
    nav: {},
    navOpen: false,
    mobNav: {},
    settings: {},
    page: {},
    pageMeta: {},
    activeMap: {},
    activeModal: null,
    $mainRouter: null
  },
  getters: {
    baseURL: state => state.baseURL,
    lang: state => state.lang,
    loading: state => state.loading,
    gate: state => state.gate,
    cookies: state => state.cookies,
    about: state => state.about,
    beverages: state => state.beverages,
    allBeverages: state => state.allBeverages,
    categories: state => state.categories,
    news: state => state.news,
    filters: state => state.filters,
    filterLabelMap: state => state.filterLabelMap,
    filterValueMap: state => state.filterValueMap,
    positions: state => state.positions,
    pages: state => state.pages,
    ui: state => state.ui,
    nav: state => state.nav,
    navOpen: state => state.navOpen,
    mobNav: state => state.mobNav,
    settings: state => state.settings,
    route: state => state.route,
    page: state => state.page,
    activeMap: state => state.activeMap,
    activeModal: state => state.activeModal,
    pageMeta: state => state.pageMeta
  },
  actions: {
    switchLoading({ commit }, l) {
      commit('SET_LOADING', l)
    },
    agreeToCookies({ commit }) {
      // localStorage.setItem('cookies', JSON.stringify(true))
      sessionStorage.setItem('cookies', JSON.stringify(true))
      commit('SET_AGREED')
    },
    toggleNavState({ commit }, n) {
      if (n) {
        this.dispatch('scrollFix')
      } else {
        this.dispatch('scrollUnFix')
      }
      commit('SET_NAV_STATE', n)
    },
    setCurrentPage({ commit }, p) {
      commit('SET_NAV_STATE', false)
      commit('SET_PAGE', p)
      commit('SET_PAGE_META', typeof p.meta !== 'undefined' ? p.meta : {})
    },
    setupData({ commit }, d) {
      var beverages = {}
      var categories = {}
      var positions = {}
      commit('SET_UI', d.ui)
      commit('SET_NAV', d.nav)
      commit('SET_MOB_NAV', d.mobNav)
      commit('SET_ABOUT', d.about)
      commit('SET_FILTERS', d.filters)
      Object.keys(d.pages).forEach(i => {
        var page = d.pages[i]
        var childRoutes = []
        if (page.template === 'PageWithSubpages' && ifDefined(page.subpages)) {
          page.subpages.forEach(s => {
            var subPage = { ...s }
            subPage.path = '/' + page.slug + '/' + subPage.slug
            subPage.parent = trim({ ...page })
            childRoutes.push({
              path: subPage.path,
              name: subPage.slug,
              meta: subPage,
              component: templates[subPage['template']]
            })
          })
        }
        if (page.template === 'CareerTemplate' && ifDefined(page.positions)) {
          Object.keys(page.positions).forEach(p => {
            var position = page.positions[p]
            position.parent = trim({ ...page })
            childRoutes.push({
              path: position.slug,
              name: position.slug,
              meta: position,
              component: templates['PositionTemplate']
            })
          })
        }
        if (page !== '') {
          page.path = page.path === '/_' ? '/' : page.path
          router.addRoutes([
            {
              path: page.path,
              name: page.slug,
              meta: page,
              component: templates[page.template],
              children: childRoutes
            }
          ])
        }
      })
      var allBeverages = []
      Object.keys(d.categories).forEach(c => {
        var category = d.categories[c]
        var childRoutes = []
        Object.keys(category.beverages).forEach(b => {
          var beverage = category.beverages[b]
          beverage.path = '/' + category.slug + '/' + beverage.slug
          beverage.type = category.title
          allBeverages.push(trim(beverage))
          beverage.parent = { ...category }
          router.addRoutes([
            {
              path: beverage.path,
              name: beverage.slug,
              meta: beverage,
              component: templates['BeverageTemplate']
            }
          ])
        })
        beverages[c] = category
        router.addRoutes([
          {
            path: category.path,
            name: category.slug,
            meta: category,
            component: templates[category.template],
            children: childRoutes
          }
        ])
      })
      allBeverages.sort((a, b) => a['title'].localeCompare(b['title']))
      allBeverages = allBeverages.map(b => {
        var s = latinize(b.title)
        s = s + ' ' + latinize(b.description)
        s = s + ' ' + latinize(b.subcategory)
        s = s + ' ' + latinize(b.type)
        b.left_section.forEach(l => { s = s + ' ' + latinize(l.value) })
        b.right_section.forEach(r => { s = s + ' ' + latinize(r.value) })
        delete b.parent
        b['search'] = s
        return b
      })
      Object.keys(d.news).forEach(n => {
        var post = d.news[n]
        router.addRoutes([
          {
            path: '/' + post.slug,
            name: post.slug,
            meta: post,
            component: templates['PostTemplate']
          }
        ])
      })
      router.addRoutes([
        { path: '*', component: templates['PageNotFound'] }
      ])
      commit('SET_PAGES', d.pages)
      commit('SET_CATEGORIES', categories)
      commit('SET_ALL_BEVERAGES', allBeverages)
      commit('SET_NEWS', d.news)
      commit('SET_POSITIONS', positions)
      commit('SET_LOADING', false)
    },
    setGate({ commit }, g) {
      commit('SET_GATE', g)
      // localStorage.setItem('gate', JSON.stringify(g))
      sessionStorage.setItem('gate', JSON.stringify(g))
    },
    openMap({ commit }, m) {
      this.dispatch('openModal', 'map')
      commit('SET_MAP', m)
    },
    openModal({ commit }, m) {
      commit('SET_MODAL', m)
      this.dispatch('scrollFix')
    },
    closeModal({ commit }) {
      commit('SET_MODAL', null)
      this.dispatch('scrollUnFix')
    },
    scrollFix({ commit }) {
      var $page = document.querySelector('[data-page]')
      document.body.style.paddingRight =
        window.innerWidth - document.body.clientWidth + 'px'
      document.body.style.touchAction = 'none'
      document.body.style.overflow = 'hidden'
      document.body.style.pointerEvents = 'none'
      document.documentElement.style.paddingRight =
        window.innerWidth - document.body.clientWidth + 'px'
      document.documentElement.style.touchAction = 'none'
      document.documentElement.style.overflow = 'hidden'
      document.documentElement.style.pointerEvents = 'none'
      $page.style.paddingRight =
        window.innerWidth - document.body.clientWidth + 'px'
      $page.style.touchAction = 'none'
      $page.style.overflow = 'hidden'
      $page.style.pointerEvents = 'none'
    },
    scrollUnFix({ commit }) {
      var $page = document.querySelector('[data-page]')
      document.body.removeAttribute('style')
      document.documentElement.removeAttribute('style')
      if (ifDefined($page)) $page.removeAttribute('style')
    }
  },
  mutations: {
    SET_LOADING(state, loading) { state.loading = loading },
    SET_GATE(state, gate) { state.gate = gate },
    SET_AGREED(state) { state.cookies = true },
    SET_PAGES(state, pages) { state.pages = pages },
    SET_UI(state, ui) { state.ui = ui },
    SET_MOB_NAV(state, mobNav) { state.mobNav = mobNav },
    SET_ABOUT(state, about) { state.about = about },
    SET_BEVERAGES(state, beverages) { state.beverages = beverages },
    SET_ALL_BEVERAGES(state, allBeverages) { state.allBeverages = allBeverages },
    SET_CATEGORIES(state, categories) { state.categories = categories },
    SET_NEWS(state, news) { state.news = news },
    SET_FILTERS(state, filters) { state.filters = filters },
    SET_POSITIONS(state, positions) { state.positions = positions },
    SET_NAV(state, nav) { state.nav = nav },
    SET_NAV_STATE(state, navOpen) { state.navOpen = navOpen },
    SET_SETTINGS(state, settings) { state.settings = settings },
    SET_PAGE(state, page) { state.page = page },
    SET_MODAL(state, activeModal) { state.activeModal = activeModal },
    SET_MAP(state, activeMap) { state.activeMap = activeMap },
    SET_PAGE_META(state, pageMeta) { state.pageMeta = pageMeta }
  },
  modules: {
    device
  },
  strict: dev,
  plugins: []
})

export default store
