import 'es6-promise/auto'
import Vue from 'vue'
import axios from 'axios'
import debounce from 'debounce'
import { sync } from 'vuex-router-sync'
import Meta from 'vue-meta'
import VeeValidate from 'vee-validate'
import VueAnalytics from 'vue-analytics'

import App from '@/App'

import store from '@/store'

import router from '@/router'

import bg from '@/plugins/bg'
import thumbStyle from '@/plugins/thumbStyle'
import splitText from '@/plugins/splitText'
import mediaPath from '@/plugins/mediaPath'
import truncate from '@/plugins/truncate'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBKNPEGBc-Z5wegjs75nyMtI_1SwuvMJ54'
  }
})

Vue.use(Meta, {
  attribute: 'data-meta',
  ssrAttribute: 'data-ssr-meta'
})

Vue.use(VeeValidate, {
  classes: true
})

Vue.use(bg)
Vue.use(thumbStyle)
Vue.use(splitText)
Vue.use(mediaPath)
Vue.use(truncate)

sync(store, router)

Vue.use(VueAnalytics, {
  router,
  // id: 'UA-116565736-1',
  id: 'UA-92022013-1',
  checkDuplicatedScript: true
})

Vue.config.productionTip = false
Vue.config.silent = process.env.NODE_ENV === 'production'

// if (typeof localStorage.data !== 'undefined') {
//   store.dispatch('setupData', JSON.parse(localStorage.data))
// }

// if (typeof localStorage.cookies !== 'undefined') {
//   store.dispatch('agreeToCookies', JSON.parse(localStorage.cookies))
// }

// if (typeof localStorage.gate !== 'undefined') {
//   store.dispatch('setGate', JSON.parse(localStorage.gate))
// }

if (typeof sessionStorage.cookies !== 'undefined') {
  store.dispatch('agreeToCookies', JSON.parse(sessionStorage.cookies))
}

if (typeof sessionStorage.gate !== 'undefined') {
  store.dispatch('setGate', JSON.parse(sessionStorage.gate))
}

// var endPoint = process.env.NODE_ENV !== 'production'
//   ? store.getters.baseURL + 'static/api.json'
//   : 'http://degtine.devprojects.lt/endpoint/lt'

// var endPoint = 'http://degtine.local/endpoint/lt'
var endPoint = store.getters.baseURL + '/endpoint/' + store.getters.lang

axios.get(endPoint)
  .then(r => {
    store.dispatch('setupData', r.data)
    // console.log(r.data)
  })
  .catch(e => {
    console.log(e)
  })

store.dispatch('deviceCheck')
window.onresize = debounce(() => {
  store.dispatch('deviceCheck')
}, 200)

var initApp = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
  // template: '<App/>',
  // components: { App }
})

export default initApp
