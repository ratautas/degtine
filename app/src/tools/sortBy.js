// sorts Object or Array by provided number key
// if Object is provided it is returned as an Array
//
// usage:
// var newObject = sortBy(object/array, sortKey)

import trim from '@/tools/trim'

const sortBy = (array, key) => {
  var newArray = trim(array)
  if (typeof newArray.length === 'undefined') {
    var objectArray = []
    Object.keys(newArray).forEach(e => {
      objectArray.push(newArray[e])
    })
    newArray = objectArray
  }
  newArray.sort((a, b) => {
    return a[key] - b[key]
  })
  return newArray
}

export default sortBy
