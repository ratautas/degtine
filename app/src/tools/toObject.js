// converts Array to Object by provided key
//
// usage:
// var newArray = toObject(array, objectKey)
import trim from '@/tools/trim'

const toObject = (array, key) => {
  var newObject = {}
  array.forEach(e => {
    newObject[e[key]] = e
  })
  return newObject
}

export default toObject
