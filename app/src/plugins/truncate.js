export default {
  install: Vue => {
    Vue.prototype.truncate = (text, length) => {
      if (text.length < length) {
        return text
      }
      length = length - 3
      return text.substring(0, length) + '...'
    }
  }
}
