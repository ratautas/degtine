var mediaUrl = document.body.getAttribute('data-page-url') + '/storage/app/media'

export default {
  install: Vue => {
    Vue.prototype.thumbStyle = (url, color) => {
      return {
        'background-image': 'url(' + encodeURI(mediaUrl + url) + ')',
        'background-color': color
      }
    }
  }
}
