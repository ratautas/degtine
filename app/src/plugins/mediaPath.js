var mediaUrl = document.body.getAttribute('data-page-url') + '/storage/app/media'

export default {
  install: Vue => {
    Vue.prototype.mediaPath = url => {
      return encodeURI(mediaUrl + url)
    }
  }
}
