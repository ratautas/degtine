var mediaUrl = document.body.getAttribute('data-page-url') + '/storage/app/media'

export default {
  install: Vue => {
    Vue.prototype.bg = url => {
      return { 'background-image': 'url(' + encodeURI(mediaUrl + url) + ')' }
    }
  }
}
