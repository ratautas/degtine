import { mapGetters, mapActions } from 'vuex'
import ifDefined from '@/tools/ifDefined'

export default {
  data() {
    return {}
  },
  computed: {
    ...mapGetters(['baseURL', 'ui', 'page', 'pageMeta'])
  },
  metaInfo() {
    var ui = this.ui
    var suffix = ifDefined(ui.page_suffix)
    var title = ifDefined(this.page.meta_title)
      ? this.page.meta_title + suffix
      : ui.default_meta_title + suffix
    var description = ifDefined(this.page.meta_description)
      ? this.page.meta_description
      : ui.default_meta_description
    var image = ifDefined(this.page.meta_image) ? this.page.meta_image : ui.default_meta_image
    return {
      title: title,
      meta: [
        { property: 'og:type', content: 'website' },
        { name: 'description', content: description },
        { property: 'og:title', content: title },
        { property: 'og:description', content: description },
        { property: 'og:url', content: this.baseURL + this.page.path },
        { property: 'og:image', content: image }
      ]
    }
  },
  updated() { },
  methods: {
    ...mapActions([])
    // ...mapActions(['setCurrentPage']),
  },
  mounted() { }
}
